% function sim1LineHTP
% from Damien's files ~_Damien/BNL/HTP

fig = figure;
dcm_obj = datacursormode(fig);
set(dcm_obj,'UpdateFcn',@NewCallback)

set(gcf,'PaperUnits','centimeters');
set(gcf,'Position',[50 100 700 500]);
%}

results = load('results.dat','rt');

%results = load(strcat(pwd,'/Data_C2H2/','Line155_1'),'rt');

%results = load('test.txt','rt');


%plot(results(:,1),results(:,2),'b')
%hold on
%plot(results(:,1),results(:,3),'r')
%hold on
%plot(results(:,1),results(:,4),'g')
%hold on
%plot(results(:,1),results(:,5),'k')

%%%%%%%%%%%%%Generation of the lineshape for test%%%%%%%%%%%%%%%%%%%%%%%%%%

%Parameters for HTP
%{
% Water...
T=100;              	%temperature K
amM1 = 18.02;           %molar mass of the absorber g/mol
Gam0 = 0.1;             %speed averaged line width
Shift0 = 0.1e-2;        %speed averaged of the line shift
sg0 = 1000;            	%center line position
Gam2 = 0.0e-1;          %speed dependence of the line width
Shift2 = 0.0e-4;        %speed dependence of the line shift
anuVC = 0.0e-1;         %velocity changing frequency
eta = 0.0;              %correlation parameter
Amp = 1;
%}
%
T=295;              	%temperature K
pressure = 1/760;                    %in atm.
amM1 = (12+1.00782503207)*2;         %molar mass of the absorber g/mol
Gam0 = 0.157*pressure;             %speed averaged line width
Shift0 = 0.0*pressure; %-0.005*pressure;        %speed averaged of the line shift
sg0 = 6578.5759;            	%center line position
Gam2 = 0.0*pressure;          %speed dependence of the line width
Shift2 = 0.0*pressure;        %speed dependence of the line shift
anuVC = 0.0e-1;         %velocity changing frequency
eta = 0.0;              %correlation parameter
% Amp = -0.000533;
Amp = -0.01661*pressure;  % amplitude factor for R(9) at 296 K and 38 cm path.
%
%
cl = 299792458;       	%speed of light
kb = 1.3806488e-23;   	%Boltzmann constant
h=6.626068e-34;       	%Planck's constant
uma=1.660538921e-27;  	%atomic mass unit
M = amM1*uma;           %mass of absorber in kg

FPrec = 1e-11;

w      = results(:,1);
%w      = 995:0.001:1005;
HTPsim = results(:,2);

indw = 3;
%w      = results(:,indw);
%HTPsim = results(:,1);

%GamD = sqrt(2*log(2)*kb*T/(M*cl*cl))*sg0;
va0  = sqrt(2*kb*T/M);
cte = cl/(sg0*va0);
%GamD = 3.581163139e-7*sg0*sqrt(T/amM1);
%va0 = cl*GamD./(sqrt(log(2)).*sg0);


C0 = Gam0 + 1i*Shift0;
C2 = Gam2 + 1i*Shift2;


C0m = (1-eta)*(C0-3*C2/2) + anuVC;
C2m = (1-eta)*C2;

X = (1i*(sg0-w)+C0m)./C2m;
Y = (sg0.*va0./(2*cl*C2m)).^2;

if C2m ==0
    Zm = (1i*(sg0-w)+C0m)*cte;
    FadZm = Faddeyeva(1i*Zm,FPrec);
    BHTP = sqrt(pi)*cte*((1-Zm.^2).*FadZm + Zm/sqrt(pi));
    AHTP = sqrt(pi)*cte*FadZm;
    LineTop = AHTP;
    LineBot = 1 - (anuVC-eta*(C0-3*C2/2)).*AHTP + eta*C2.*BHTP;
else
    
    Zp = sqrt(X+Y) + sqrt(Y);
    Zm = sqrt(X+Y) - sqrt(Y);

    %One time calculation of the Faddeyeva functions
    FadZm = Faddeyeva(1i*Zm,FPrec);
    FadZp = Faddeyeva(1i*Zp,FPrec);

    BHTP = va0.^2./C2m.*(-1 + sqrt(pi)*(1-Zm.^2).*FadZm./(2*sqrt(Y)) - ...
       sqrt(pi)*(1-Zp.^2).*FadZp./(2*sqrt(Y)));

    AHTP = sqrt(pi)*cl*(FadZm - FadZp)./(sg0.*va0);

    LineTop = AHTP;
    LineBot = 1 - (anuVC-eta*(C0-3*C2/2)).*AHTP + (eta*C2./(va0.^2)).*BHTP;
end

%LineGen = exp(Amp/pi * real(LineTop./LineBot));

LineGen = Amp/pi * real(LineTop)

rmse = sum((HTPsim-LineGen).^2)/length(w);

plot(w,LineGen)
hold on
plot(w,HTPsim,'k')
%plot(w,HTPsim-LineGen,'k')

%trapz(w,LineGen)

%%%%%%%%%%%%End generation of the lineshape%%%%%%%%%%%%%%%%%%%%%%%%%%%
















