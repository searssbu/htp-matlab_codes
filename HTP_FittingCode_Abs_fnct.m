function [gammas,fitgams,deltas, fitdels] = ...
                            HTP_FittingCode_Abs_fnct(transitions)
% !synclient HorizTwoFingerScroll=0   % If you have problems with mousepad
% ========== This version fits to Absorbance line profiles ===========
%clearvars;
%close all;
%The program first generates a HTP and then attempts to fit it.
%The purpose of this code is to illustrate how to fit a Absorbance HTP 
%
%Work based on 
%Tennyson et al., Pure Appl. Chem., 86 (12), 1931-1943 (2014)
%H. Tran, N. H. Ngo, and J.-M. Hartmann, J. Quant. Spectrosc. Radiat. Transf. 129, 199 (2013).
%N. H. Ngo, D. Lisak, H. Tran, and J.-M. Hartmann, J. Quant. Spectrosc. Radiat. Transf. 129, 89 (2013).

%Faddeyeva function computed from code by M. R. Zaghloul & A. N. Ali
%ACM Trans. Math. Softw., 38,2, article 15, 2011
%http://doi.acm.org/10.1145/2049673.2049679
%DOI: 10.1145/2049673.2049679
% Parameter Names character string
ParamNames={' Gam0 ';'Shift0';' Gam2 ';'Shift2';' anuVC';'  eta ';' Ampl '};
cl = 299792458;                 %speed of light
kb = 1.3806488e-23;             %Boltzmann constant
uma=1.660538921e-27;            %atomic mass unit
ln10=log(10.0);
% ln10=1.0;
%
FPrec = 1e-11;                  %precision of the Faddeyeva function
%
%%  Set up the data 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% transdata=cellstr(transitions);                 %convert to string array
S_vals = [1.303e-20 4.465e-21 1.340e-20 4.380e-21 1.264e-20 3.977e-21 1.107e-20...
    3.369e-21];     % Hitran Line Strengths
% *************************************************************************
% Artifically reduce the line strngths to account for amplitide
% representation transmittance instead of the normal power.
   S_vals = 0.5*S_vals;
% *************************************************************************
freqs = [6574.361355 6576.481658 6578.575894 6580.644073 6582.686025 6584.701858 ...
    6586.691493 6588.654889];     % Line rest frequencies
gammas = [0.162 0.158 0.154 0.150 0.147 0.144 0.141 0.138];  % average broadening 
                                      % coefficient in cm-1/atm
deltas = [-0.0071 -0.0077 -0.0082 -0.0088 -0.0093 -0.0098 -0.0103 -0.0108]; 
                           % average pressure shift coefficient in cm-1/atm
gam2s = [0.0212 0.0212 0.0212 0.0212 0.0212 0.0212 0.0212 0.0212];
                                      % quadratic speed correction to gamma
del2s = [-0.00132 -0.00132 -0.00132 -0.00132 -0.00132 -0.00132 -0.00132 -0.00132];
                                      % quadratic shift correction to delta
numtrans = numel(freqs) ;      % cut number of transitions here, if desired
%
% fixParam is a vector referencing fixed variables
% for example, input fixParam = [2 4] if you want to
% fix variables Delta0 and Delta2 to their defined values
fixParam = [3 4 5 6 7];
%
%
%
%%   Generation of the lineshape for test  
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Gas Parameters for HTP
nL=2.47937269e19;               % Loschmidt's number for 1 atm per cm3 at 296K
nL=nL/760;                      % convert to 1 torr pressure
T=296;                          %temperature K  (296)
nL=nL*(296/T);                  % adjust nL for Temp 
amM1 = (12+1.00782503207)*2;   	%molar mass of the absorber g/mol example with acetylene
M = amM1*uma;                   %mass of absorber in kg
%
noiseampl = 0.008;              % +/- noise ampl in absorbance spectrum
%{
pressvals = [1.323 2.295 2.400 4.253 20.0];  % Deng et al R(9) 4.0 cm cell
   % pressures at which the calculation will be done in torr
pthlengths = [4.0 4.0 4.0 4.0 4.0 4.0];   % cell lengths in cms for above pressures
%}
pressvals = [0.188 0.450 2.97 7.853 14.72 19.91];
pthlengths = [150 150 50 50 20 20];
numpress = numel(pressvals);
nparmstot=7;                    % length of parameter arrays (7 for HTP)
origparm = zeros(nparmstot,numpress,numtrans);  % for saving initial parameters
finalparm = zeros(nparmstot,numpress,numtrans); % for saving final fitted parameters
rmsesave = zeros(numpress, numtrans);       % for saving the rmse of the fits
%
%
prof_test=zeros(1,numpress);    % test store of profile areas
%% start loop over transtions
% endloop=numtrans;prof_test
%
for i_trans = 1 : numtrans
    sg0 = freqs(i_trans);
    SvN = S_vals(i_trans);
    % tchar = transdata{i_trans};             % choose the transition ID
% start looop over pressures
for i_press = 1:numpress     % loop over pressures
pressure=pressvals(i_press)/760;                % Pressure in atm (torr/760)
%%%%%%%%%%%%%%%%%%  Parameters for HTP  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Gam0 = gammas(i_trans)*pressure;   % most prob. speed line width
Shift0 = deltas(i_trans)*pressure; % -0.001;        % Mot prob.speed line shift
Gam2 = gam2s(i_trans)*pressure;    % speed dependence of the line width
Shift2 = del2s(i_trans)*pressure;   % speed dependence of the line shift
anuVC = 0.0;                    % 0.004;       %velocity changing frequency
eta = 0.0;                      % 0.2          %correlation parameter
% SvN = S_vals(2);                % choose the right Hittan S-value
Amp = SvN*nL*pthlengths(i_press)*pressure*760; %     %amplitude of the signal 
                                % Defined as a negative quantity!
                                %-> Related to line strength, defined
                                % with a negative sign\at 296K and
                                % pthlength(i_trans)  cm long cell. 
                                %
                                % Note, 10 torr in 38 cm cell is black
                                % at line center for R(9)
% Amp0=Amp
%w      = (sg0-0.32:0.001:sg0+0.32).';    %frequency axis
startaxis = sg0-0.20;   %6528.85 for P(11) 
endaxis = sg0+0.20;     %6529.40
w      = (startaxis:0.003:endaxis).';    % Array of wavenumber points for the 
                                 % calculaton step 0.002 normally     
%GamD = sqrt(2*log(2)*kb*T/(M*cl*cl))*sg0;  %Doppler width
va0  = sqrt(2*kb*T/M);                      %most probable speed of molecules with mass M
cte = cl/(sg0*va0);                         %constant used in the case C2t = 0
%GamD = 3.581163139e-7*sg0*sqrt(T/amM1);    %equivalent to above
%va0 = cl*GamD./(sqrt(log(2)).*sg0);        %equivalent to above


C0 = complex(Gam0, Shift0); %multipy by one*1 Gam0 + 1i*Shift0;  
C2 = complex(Gam2, Shift2); %Gam2 + 1i*Shift2;   


%C0 tilde and C2 tilde are denoted with a t
C0t = (1-eta)*(C0-3*C2/2) + anuVC;
C2t = (1-eta)*C2;

%C2t = 0 is important for generation of Voigt profiles
%and is a special case
%if C2t=0, then the fit should not let float eta, Gamma_2 and Delta_2
if C2t == 0
    Zm = (1i*(sg0-w)+C0t)*cte;
    FadZm = Faddeyeva(1i*Zm,FPrec);
    BHTP = sqrt(pi)*cte*((1-Zm.^2).*FadZm + Zm/sqrt(pi));
    AHTP = sqrt(pi)*cte*FadZm;
    LineTop = AHTP;
    LineBot = 1 - (anuVC-eta*(C0-3*C2/2)).*AHTP + eta*C2.*BHTP;
else
    X = (1i*(sg0-w)+C0t)./C2t;
    Y = (sg0.*va0./(2*cl*C2t)).^2;
%  These are divide by zero  if C2t is zero  ******
    Zp = sqrt(X+Y) + sqrt(Y);
    Zm = sqrt(X+Y) - sqrt(Y);
    
    %One time calculation of the Faddeyeva functions
    FadZm = Faddeyeva(1i*Zm,FPrec);
    FadZp = Faddeyeva(1i*Zp,FPrec);
    
    BHTP = va0.^2./C2t.*(-1 + sqrt(pi)*(1-Zm.^2).*FadZm./(2*sqrt(Y)) - ...
        sqrt(pi)*(1-Zp.^2).*FadZp./(2*sqrt(Y)));
    
    AHTP = sqrt(pi)*cl*(FadZm - FadZp)./(sg0.*va0);
    
    LineTop = AHTP;
    LineBot = 1 - (anuVC-eta*(C0-3*C2/2)).*AHTP + (eta*C2./(va0.^2)).*BHTP;
    
end
np = real(LineTop./LineBot)/pi;
% Synthetic absorbance lineshape with normalization factor
  LineGen = Amp * np/ln10;        % Absorbance in log10  line shape 
  dAdT = 1./exp(ln10*LineGen)*(1/ln10);  % for converting noise in T to A
%
prof_test(i_press) = trapz(w,np);
% add some random noise to the Transmission signal
rnoise = -noiseampl+2*noiseampl.*rand(size(w));
LineGen=LineGen + dAdT.*rnoise;   % Convert to noise in Absorbance units
% tchar=transdata(i_trans);
%{
% Dunp the synthetic line to a text file
outvar = [w(:),LineGen(:)];
outvar=outvar';
fileID=fopen('HTPtest.txt','w');
fprintf(fileID,'%12.6f  %12.6f \n', outvar(:));
fclose(fileID);
%}
% generate text for title label
% pchar=num2str(pressure*760);
% lchar=num2str(pthlengths(i_press));
% titel=strcat(tchar,':',lchar,' cm:', pchar,' torr');
%{
% Plot the prediction  
    figure
    plot(w,LineGen,'b-')
    title(titel)
    xlabel('Frequency axis');
    ylabel('Absorbance');
%}
%% Output the profile data 
%B is the vector containing the variables
%     1     2      3    4     5     6   7
B = [Gam0 Shift0 Gam2 Shift2 anuVC eta Amp];
origparm(:,i_press,i_trans)=B;      % save the parameters for this pressure
%fprintf('The initial parameters\n');
%fprintf('%10.6f,%10.6f,%10.6f,%10.6f,%10.6f,%10.6f,%10.6f\n',B);
%filename = char(strcat(tchar,pchar,'torr_profileD.txt'));
% DataOut = [w';LineGen'];
%fileID = fopen(filename,'w');
%fprintf(fileID, '%4.1f, %6.1f\n',pressure*760, T);
%fprintf(fileID,'%10s %10s %10s %10s %10s %10s %10s\n ',ParamNames{1},ParamNames{2},...
%    ParamNames{3},ParamNames{4},ParamNames{5},ParamNames{6},ParamNames{7});
%fprintf(fileID,'%10.6f,%10.6f,%10.6f,%10.6f,%10.6f,%10.6f,%10.6f\n', B);
% fprintf(fileID,'%4i\n',length(w));            % The number of points
% fprintf(fileID, '%9.4f  %9.5f\n', DataOut);   % The synthetic data
%fclose(fileID);
%
% end ot output of profile data ==========================================
%
%%%%%%%%%%%% End generation of the lineshape%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Fit the synthetic Line Profile to some approximate function
%
% Initialization of parameters These are close to, but not exactly the
% values in the set up 
%speed averaged line width
Gam0 = 1.02*gammas(i_trans)*pressure ; 
%speed averaged of the line shift
Delta0 = 0.98*deltas(i_trans)*pressure ;
% sg0    = freqs(2); 	                %center line position
%speed dependence of the line width
Gam2   = 0.0; %gam2s(i_trans)*pressure;  % speed dependence of gamma
Delta2 = 0.000;         %speed dependence of the line shift
nuVC   = 0.000;         %velocity changing frequency
eta    = 0.0;           %correlation parameter
% the 2 parameters above are aet to zero for a QSDV model
%Amplitude of the signal Positive here for absorbance
Amp = SvN*1.0*nL*pthlengths(i_press)*pressure*760;  % slight adjustment to SvN
% Amp1=Amp
%
%B is the vector containing the variables
%     1     2      3    4     5     6   7
B = [Gam0 Delta0 Gam2 Delta2 nuVC eta Amp];

%fixParam is a vector referencing fixed variables
%for example, input fixParam = [2 4] if you want to
%fix variables Delta0 and Delta2 to their defined values
% ====== It is set near the top of the script ======
% fixParam = [3 4 5 6 7];
lengthw = length(LineGen);
lengthB = length(B);

F = zeros(lengthw,1);               %storage of Obs-Cal
Ic = zeros(lengthw,1);              %generated lineshape at each step
IcA = zeros(lengthw,1);             %generated lineshape at each step
Icref = LineGen;                    %original lineshape

for inc = 1:10                      %number of steps in minimization
                                    %-> can be modified to stop when
                                    %convergence achived

    % update of the variables at each step
    va0  = sqrt(2*kb*T/M);
    Gam0   = B(1);
    Shift0 = B(2);
    Gam2   = B(3);
    Shift2 = B(4);
    anuVC  = B(5);
    eta    = B(6);
    Amp    = B(7);
    % Amp1=Amp    
    J=zeros(lengthw,lengthB);   %Jacobian
    %Incre = zeros(1,lengthB);  	%Vector incrementing B at each step
    
    C0 = Gam0 + 1i*Shift0;
    C2 = Gam2 + 1i*Shift2;
    
    C0t = (1-eta)*(C0-3*C2/2) + anuVC;
    C2t = (1-eta)*C2;    
    
    if C2t == 0        
%        cte = cl./(sg0*va0);
        Zm = (1i*(sg0-w)+C0t).*cte;
        FadZm = Faddeyeva(1i*Zm,FPrec);
        BHTP = sqrt(pi)*cte.*((1-Zm.^2).*FadZm + Zm/sqrt(pi));
        AHTP = sqrt(pi)*cte.*FadZm;
        LineTop = AHTP;
        LineBot = 1 - (anuVC-eta*(C0-3*C2/2)).*AHTP + eta*C2.*BHTP;
        
        Line = 1/pi*real(LineTop./LineBot);
        
        %%%%%%%%%%derivatives%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        dwdZm     = (2*Zm.*FadZm - 2/sqrt(pi));
        dZdC0t     = cte;
        
        dAdGam0    = sqrt(pi)*cte.*dwdZm.*dZdC0t.*(1-eta);
        dAdGam2    = -sqrt(pi)*cte.*dwdZm.*dZdC0t.*(1-eta)*1.5;
        dAdanuVC   = sqrt(pi)*cte.*dwdZm.*dZdC0t;
        dAdeta     = -sqrt(pi)*cte.*dwdZm.*dZdC0t.*(C0-3*C2/2);
        
        dBdGam0    = sqrt(pi)*cte.*(-2*Zm.*FadZm + (1-Zm.^2).*dwdZm +...
            1/sqrt(pi)).*dZdC0t.*(1-eta);
        dBdGam2    = sqrt(pi)*cte.*(-2*Zm.*FadZm + (1-Zm.^2).*dwdZm + 1/sqrt(pi)).*dZdC0t.*(-(1-eta)*1.5);
        dBdanuVC   = sqrt(pi)*cte.*(-2*Zm.*FadZm + (1-Zm.^2).*dwdZm + 1/sqrt(pi)).*dZdC0t;
        dBdeta     = -sqrt(pi)*cte.*(-2*Zm.*FadZm + (1-Zm.^2).*dwdZm + 1/sqrt(pi)).*dZdC0t.*(C0-3*C2/2);
        
        
        dLineBotdGam0 = -(dAdGam0.*(anuVC - eta.*(C0-3*C2/2)) - AHTP.*eta) + eta.*C2.*dBdGam0;
        dLinedGam0   = Amp/pi.*(dAdGam0.*LineBot - AHTP.*dLineBotdGam0)./(LineBot.^2);
        
        dLinedShift0   = dLinedGam0*1i;
        
        dLineBotdGam2 = -(dAdGam2.*(anuVC - eta.*(C0-3*C2/2)) + 3/2*AHTP.*eta) + eta.*(C2.*dBdGam2 + BHTP);
        dLinedGam2   = Amp/pi.*(dAdGam2.*LineBot - AHTP.*dLineBotdGam2)./(LineBot.^2);

        dLinedShift2   = dLinedGam2*1i;
        
        dLineBotdeta = -(dAdeta.*(anuVC - eta.*(C0-3*C2/2)) - AHTP.*(C0-3*C2/2)) + C2.*(eta.*dBdeta + BHTP);
        dLinedeta   = Amp/pi.*(dAdeta.*LineBot - AHTP.*dLineBotdeta)./(LineBot.^2);
        
        dLineBotdanuVC = -(dAdanuVC.*(anuVC - eta.*(C0-3*C2/2)) + AHTP) + C2.*eta.*dBdanuVC;
        dLinedanuVC   = Amp/pi.*(dAdanuVC.*LineBot - AHTP.*dLineBotdanuVC)./(LineBot.^2);
        
    else
        
        X = (1i*(sg0-w)+C0t)./C2t;
        Y = (sg0.*va0./(2*cl*C2t)).^2;
        
        Zp = sqrt(X+Y) + sqrt(Y);
        Zm = sqrt(X+Y) - sqrt(Y);
        
        FadZm = Faddeyeva(1i*Zm,FPrec);
        FadZp = Faddeyeva(1i*Zp,FPrec);
        
        BHTP = va0.^2./C2t.*(-1 + sqrt(pi)*(1-Zm.^2).*FadZm./(2*sqrt(Y)) - ...
            sqrt(pi)*(1-Zp.^2).*FadZp./(2*sqrt(Y)));
        
        AHTP = sqrt(pi)*cl*(FadZm - FadZp)./(sg0.*va0);
        
        LineTop = AHTP;
        LineBot = 1 - (anuVC-eta*(C0-3*C2/2)).*AHTP + (eta*C2./(va0.^2)).*BHTP;
        
        Line = 1/pi*real(LineTop./LineBot);

        %%%%%%%%Constants for derivatives%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        cteFacA  = sqrt(pi)*cl./sg0./va0;
        cteFacB  = va0.^2*sqrt(pi)./(C2t.*2.*sqrt(Y));
        
        dwdZm   = (2*Zm.*FadZm - 2/sqrt(pi));
        dwdZp   = (2*Zp.*FadZp - 2/sqrt(pi));
        
        dZdC0t   = 1./(C2t.*2.*sqrt(X+Y));
        dZmdC2t = (1./(2*sqrt(X+Y))).*(-X./C2t - 2*Y./C2t) - 1./(2*sqrt(Y)).*(-2*Y./C2t);
        dZpdC2t = (1./(2*sqrt(X+Y))).*(-X./C2t - 2*Y./C2t) + 1./(2*sqrt(Y)).*(-2*Y./C2t);
        
        
        dZdGam0 = ((1./(2*sqrt(X+Y))).*((1-eta)./C2t));
        
        dC2tdC2  = 1-eta;
        dC2tdeta = -C2;
        dC0tdC2  = -3/2*(1-eta);
        dC0tdeta = -(C0-3*C2/2);
        
        
        %%%%%%%%%%%%%Derivatives%%%%%%%%%%%%%%%%%%%%%%
        
        
        dAdGam0   = cteFacA.*dZdGam0.*(dwdZm - dwdZp);
        
        dAdGam2   = cteFacA.*(dwdZm.*(dZmdC2t.*dC2tdC2 + dZdC0t.*dC0tdC2) - ...
            dwdZp.*(dZpdC2t.*dC2tdC2  + dZdC0t.*dC0tdC2));
        
        dAdeta    = cteFacA.*(dwdZm.*(dZmdC2t.*dC2tdeta + dZdC0t.*dC0tdeta) - ...
            dwdZp.*(dZpdC2t.*dC2tdeta  + dZdC0t.*dC0tdeta));
        
        dAdanuVC  = cteFacA.*(dwdZm - dwdZp).*dZdC0t;
        
        
        
         dBdGam0   = cteFacB.*...
             (-2*Zm.*dZdGam0.*FadZm + (1-Zm.^2).*dwdZm.*dZdGam0 - ...
             (-2*Zp.*dZdGam0.*FadZp + (1-Zp.^2).*dwdZp.*dZdGam0));
         
         
         dBdGam2  = -BHTP./C2t.*dC2tdC2 +...
             cteFacB.*(1./C2t .* dC2tdC2 .* ((1-Zm.^2).*FadZm -(1-Zp.^2).*FadZp) + ...
             (dZmdC2t.*dC2tdC2 + dZdC0t.*dC0tdC2).*(-2*Zm.*FadZm + (1-Zm.^2).*dwdZm) - ...
             (dZpdC2t.*dC2tdC2 + dZdC0t.*dC0tdC2).*(-2*Zp.*FadZp + (1-Zp.^2).*dwdZp));

         
         dBdeta    = -BHTP./C2t.*dC2tdeta +...
             cteFacB.*(1./C2t .* dC2tdeta .* ((1-Zm.^2).*FadZm -(1-Zp.^2).*FadZp) +...
             (dZmdC2t.*dC2tdeta + dZdC0t.*dC0tdeta).*(-2*Zm.*FadZm + (1-Zm.^2).*dwdZm) - ...
             (dZpdC2t.*dC2tdeta + dZdC0t.*dC0tdeta).*(-2*Zp.*FadZp + (1-Zp.^2).*dwdZp));
         
         dBdanuVC  = cteFacB.*(-2*Zm.*dZdC0t.*FadZm + (1-Zm.^2).*dwdZm.*dZdC0t - ...
             (-2*Zp.*dZdC0t.*FadZp + (1-Zp.^2).*dwdZp.*dZdC0t));
         
        
        
        
        
        dLineBotdGam0 = -(dAdGam0.*(anuVC - eta.*(C0-3*C2/2)) - AHTP.*eta) + eta.*C2.*dBdGam0./va0.^2;
        dLinedGam0   = Amp/pi.*(dAdGam0.*LineBot - AHTP.*dLineBotdGam0)./(LineBot.^2);
        
        dLinedShift0   = dLinedGam0*1i;
        
        dLineBotdGam2 = -(dAdGam2.*(anuVC - eta.*(C0-3*C2/2)) + 3/2*AHTP.*eta) + eta./va0.^2.*(C2.*dBdGam2 + BHTP);
        dLinedGam2   = Amp/pi.*(dAdGam2.*LineBot - AHTP.*dLineBotdGam2)./(LineBot.^2);
        
        dLinedShift2   = dLinedGam2*1i;
        
        dLineBotdeta = -(dAdeta.*(anuVC - eta.*(C0-3*C2/2)) - AHTP.*(C0-3*C2/2)) + C2./va0.^2.*(eta.*dBdeta + BHTP);
        dLinedeta   = Amp/pi.*(dAdeta.*LineBot - AHTP.*dLineBotdeta)./(LineBot.^2);
        
        dLineBotdanuVC = -(dAdanuVC.*(anuVC - eta.*(C0-3*C2/2)) + AHTP) + C2./va0.^2.*eta.*dBdanuVC;
        dLinedanuVC   = Amp/pi.*(dAdanuVC.*LineBot - AHTP.*dLineBotdanuVC)./(LineBot.^2);
        
    end
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %  Retrieved line profile
    % IcT(1:lengthw,1)  = exp(Amp*Line);
    IcA(1:lengthw,1)  =  Amp*Line/ln10; % Correct for negative sign of Ampl in definition
    %Filling the Jacobian
    J(1:lengthw,1)  = dLinedGam0/ln10;
    J(1:lengthw,2)  = dLinedShift0/ln10;
    J(1:lengthw,3)  = dLinedGam2/ln10;
    J(1:lengthw,4)  = dLinedShift2/ln10;
    J(1:lengthw,5)  = dLinedanuVC/ln10;
    J(1:lengthw,6)  = dLinedeta/ln10;
    J(1:lengthw,7)  = (Line)/ln10;
    
    
    J = real(J);
    Ic = real(IcA);
    
    %Obs-Cal
    F = (Icref) - Ic;
    
    rmse = (sum(F.^2)/length(F))^(0.5);
    
    % fprintf('rmse = %e\n',rmse);
    J(:,fixParam)=[]; %remove columns of fixed parameters such that they do not contribute
                      %to the Matrix inversion
    % weighted least squares                  
    trans_nut = exp(-Amp*Line);            % Transmission Spectrum
    wt = 1./trans_nut.^2;                  % Weight points as 1/tr(\tnu}^2
    
    npv=size(J,2);
    JTWF=J'*(wt.*F);                             % matrix for weighted Jacobian
    Jb=zeros(npv,npv);
    for iwgt=1:npv
        for jwgt=1:npv
            for kwgt=1:lengthw
                Jb(iwgt,jwgt)=Jb(iwgt,jwgt)+J(kwgt,iwgt)*wt(kwgt)*J(kwgt,jwgt);
            end
        end
    end
    
    %Matrix inversion to get the incrementation of B
    Incre=Jb\JTWF;
    Incre = Incre.';
    
    %insert 0 in the incrementation vector where parameters are fixed
    if length(fixParam)>=1
        for u=1:length(fixParam)
            Incre(1,fixParam(u)+1:end+1)=Incre(1,fixParam(u):end);
        end
        Incre(1,fixParam)=0;
    end
    
    B = B + Incre;  %increment B
end
%% Plot and save the results for this pressure
% 
finalparm(:,i_press,i_trans)=B;
rmsesave(i_press,i_trans)=rmse;
%{
%Plot results
figure
subplot(2,1,1)
plot(w,Icref,'b')
title(titel);
hold on
plot(w,Ic,'r')
xlabel('Frequency axis')
ylabel('Absorbance')
subplot(2,1,2)
plot(w,F,'k');
xlabel('Frequency axis')
ylabel('Obs-Cal')
%}
%{
fprintf('\n');
fprintf('The final parameters\n');
fprintf('%10.6f,%10.6f,%10.6f,%10.6f,%10.6f,%10.6f,%10.6f\n',B);
fprintf(' Gam0 = %f \n Delta0 = %f \n Gam2 = %f \n Delta2 = %f \n nuVC = %f \n eta = %f \n Amp = %f \n\n',B);
%
%
% Append the results to our file for this pressure
fileID=fopen(filename,'a');
fprintf(fileID, 'Final Parameters:  rmse= %7.3e \n',rmse);
fprintf(fileID,' %10.6f %10.6f %10.6f %10.6f %10.f %10.6f %10.6f \n\n', B);
fclose(fileID);
%}
end           % of for loop over pressure values  
end            % end of the loop over the transitons
% prof_test     % print out the Faddeeva function area
%end
%%
% Now analyze the Voigtbroadening obtained from fitting to the SDv profiles
% the results are the sloes and the offsts for the Voigt gamma values 

[~,fitgams, ~, ~, fitdels, ~,~,~,~] = ...
    FitCoeffs_C(numtrans, pressvals, finalparm);
%
%% Further analysis
% We can use the 3-d arrays origparm and finalparm to compare the results
% We are particular;y interested in the gamma and the amplitude variables
% as these were varied in the Voigt fit to the more accurate line profile
% function at the pressures and pathlengths of the Iwakuni et al. data.
%
% Now the deltas
%{
figure
hold on
plot(Jvalues,deltas, 'k-^')
axis([7 14 -0.02 0.0])
err=fitdels(:,2);
errorbar(Jvalues, fitdels(:,1),err,'r*-')
xlabel('R-branch J-value');
ylabel(' \delta cm^{-1}/atm');
legend('Initial \delta','Fitted Voigt \delta')
title('Absorbance Voigt \delta with S_v^N fixed')
%}
%{
% Now the amplitudes
% Convert to Hitran SvN values
inv_nL = 1/nL;
SvNs = zeros(numtrans,numpress);
DSvNs = zeros(numtrans,numpress);
fracDSvNs = zeros(numtrans,numpress);
for i_trans=1:numtrans
    Ampls = finalparm(7,:,i_trans);
    SvNs(i_trans,:) = Ampls./pthlengths./pressvals*inv_nL;
    DSvNs(i_trans,:) = S_vals(i_trans)-SvNs(i_trans,:);
    fracDSvNs = DSvNs./S_vals(i_trans);
end
% The SvNs are the HITRAN S-values resulting from the Voigt profile fits to
% the data as a funciton of pressure. The DSvNs are the obs-calc  
figure
plot(pressvals,DSvNs(1,:))
hold on
plot(pressvals,DSvNs(2,:))
plot(pressvals,DSvNs(3,:))
plot(pressvals,DSvNs(4,:))
plot(pressvals,DSvNs(5,:))
plot(pressvals,DSvNs(6,:))
plot(pressvals,DSvNs(7,:))
plot(pressvals,DSvNs(8,:))
xlabel('Pressure/torr');
ylabel('obs-calc S_v^N');
title('Change in Effective Voigt Line Strengths')
legend('R7','R8','R9','R10','R11','R12','R13','R14','location','northwest')
%
figure
plot(pressvals,fracDSvNs(1,:))
hold on
plot(pressvals,fracDSvNs(2,:))
plot(pressvals,fracDSvNs(3,:))
plot(pressvals,fracDSvNs(4,:))
plot(pressvals,fracDSvNs(5,:))
plot(pressvals,fracDSvNs(6,:))
plot(pressvals,fracDSvNs(7,:))
plot(pressvals,fracDSvNs(8,:))
xlabel('Pressure/torr');
ylabel('fractional change in S_v^N');
title('Fracctional Change in Voigt Line Strengths')
legend('R7','R8','R9','R10','R11','R12','R13','R14','location','northwest')
%}
end







