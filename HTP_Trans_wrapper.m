%% For muliple calls of the HTP fit function
% For statistical evaluation of the error distribution 
% Things to consider:
% Here we set the range of J-values, typically J=7-14 for R-branch lines,
% but there are hard limits on the data set in the called function, so if
% you go outside these limits some changes to the function will need ot be
% made. 
clearvars;
close all;
% reset the random number generator, for generating the random noise
rng('shuffle');
% Set the transitions
transitions = ['R7 ' ; 'R8 '; 'R9 '; 'R10'; 'R11'; 'R12'; 'R13'; 'R14' ];
Jvalues = [7 8 9 10 11 12 13 14];

%% Call the function
numloops=256;
gamstor=zeros(numloops,size(Jvalues,2),2);
delstor=zeros(numloops,size(Jvalues,2),2);
g2stor=zeros(numloops, size(Jvalues,2),2);
%
for i=1:numloops
     [gammas,fitgams,deltas, fitdels, gam2s, fitg2s] = ...
                            HTP_FittingCode_Trans_fnct(transitions);
% store the fitted parameters
     gamstor(i,:,:)=fitgams(:,:);
     delstor(i,:,:)=fitdels(:,:);
     g2stor(i,:,:)=fitg2s(:,:);
end
%% Plots
% We are particular;y interested in the gamma and the amplitude variables
% as these were varied in the Voigt fit to the more accurate line profile
% function at the pressures and pathlengths of the Iwakuni et al. data.
% Also, find the mean values and the deviations...
storsiz=size(gamstor);
numtrans=storsiz(2);
%
gamave=zeros(numtrans,1);
gamdev=zeros(numtrans,1);
rmsqdg=zeros(numtrans,1);
gamsq=zeros(numloops, numtrans);
%
delave=zeros(numtrans,1);
deldev=zeros(numtrans,1);
rmsqdd=zeros(numtrans,1);
delsq=zeros(numloops, numtrans);
%
g2ave=zeros(numtrans,1);
g2dev=zeros(numtrans,1);
rmsqdg2=zeros(numtrans,1);
g2sq=zeros(numloops,numtrans);
%
sqdev=zeros(numloops,numtrans);
for i=1:numtrans
    % for gamma value analysis
    gamave(i)=sum(gamstor(:,i,1))/numloops;
    gamdev(i)=sum(gamstor(:,i,2))/numloops;
    % for mean square deviation calculation
    gamsq(:,i)=gamstor(:,i,1).^2;
    sqdev(:,i)=(gamsq(:,i)-gamave(i)^2);
    rmsqdg(i)=sqrt(sum(sqdev(:,i))/numloops);
    % for delta values analysis
    delave(i)=sum(delstor(:,i,1))/numloops;
    deldev(i)=sum(delstor(:,i,2))/numloops;
    % for mean square deviation calculation
    delsq(:,i)=delstor(:,i,1).^2;
    sqdev(:,i)=(delsq(:,i)-delave(i)^2);
    rmsqdd(i)=sqrt(sum(sqdev(:,i))/numloops);
    % for g2 values analysis
    g2ave(i)=sum(g2stor(:,i,1))/numloops;
    g2dev(i)=sum(g2stor(:,i,2))/numloops;
    % for mean square deviation calculation
    g2sq(:,i)=g2stor(:,i,1).^2;
    sqdev(:,i)=(g2sq(:,i)-g2ave(i)^2);
    rmsqdg2(i)=sqrt(sum(sqdev(:,i))/numloops);
end
%{
figure   % for averaged error estimates
    hold on
    plot(Jvalues,gammas,'k^-')
    errorbar(Jvalues, gamave, gamdev,'r*-')
    xlabel('R-branch J-value');
    ylabel(' \gamma cm^{-1}/atm');
    legend('Initial \gamma','Ave Fit Voigt \gamma')
    title('Average Fitted Voigt \gamma with S_v^N fixed') 
%}
    % Mean square deviations
figure
    hold on
    plot(Jvalues,gammas,'k^-')
    errorbar(Jvalues, gamave, rmsqdg,'r*-')
    xlabel('R-branch J-value');
    ylabel(' \gamma / (cm^{-1}/atm)');
    legend('Initial \gamma','Ave Fit Voigt \gamma')
    title('Average Voigt \gamma (Transmittance), Hitran S_v^N') 
    % Now the deltas 
figure
    hold on
    plot(Jvalues,deltas, 'k-^')
    axis([7 14 -0.02 0.0])
    err=rmsqdd;         % RMS average errors
   % err=deldev;           % average errors returned from fits
    errorbar(Jvalues, fitdels(:,1),err,'r*-')
    xlabel('R-branch J-value');
    ylabel(' \delta / (cm^{-1}/atm)');
    legend('Initial \delta','Fitted Voigt \delta')
    title('Fitted Voigt \delta , Hitran S_v^N') 
    % now the g2s
figure
    hold on
    plot(Jvalues,gam2s, 'k-^')
    axis([7 14 -0.02 0.06])
    err=rmsqdg2;         % RMS average errors
   % err=deldev;           % average errors returned from fits
    errorbar(Jvalues, fitg2s(:,1),err,'r*-')
    xlabel('R-branch J-value');
    ylabel(' \gamma_2 cm^{-1}/atm');
    legend('Initial \gamma_2','Fitted \gamma_2')
    title('Fitted \gamma_2 , Hitran S_v^N') 
    
% end
