%% For muliple calls of the HTP fit function
% For statistical evaluation of the error distribution 
% Things to consider:
% Here we set the range of J-values, typically J=7-14 for R-branch lines,
% but there are hard limits on the data set in the called function, so if
% you go outside these limits some changes to the fucntion will need ot be
% made. 
clearvars;
close all;
% reset the random number generator, for generating the random noise
rng('shuffle');
% Set the transitions
transitions = ['R7 ' ; 'R8 '; 'R9 '; 'R10'; 'R11'; 'R12'; 'R13'; 'R14' ];
Jvalues = [7 8 9 10 11 12 13 14];

%% Call the function
numloops=256;
gamstor=zeros(numloops,size(Jvalues,2),2);
delstor=zeros(numloops,size(Jvalues,2),2);
%
for i=1:numloops
     [gammas,fitgams,deltas, fitdels] = ...
                            HTP_FittingCode_Abs_fnct(transitions);
% store the fitted parameters
     gamstor(i,:,:)=fitgams(:,:);
     delstor(i,:,:)=fitdels(:,:);
end
%% Plots
% We are particular;y interested in the gamma and the amplitude variables
% as these were varied in the Voigt fit to the more accurate line profile
% function at the pressures and pathlengths of the Iwakuni et al. data.
% Also, find the mean values and the deviations...
storsiz=size(gamstor);
numtrans=storsiz(2);
gamave=zeros(numtrans,1);
gamdev=zeros(numtrans,1);
rmsqdg=zeros(numtrans,1);
gamsq=zeros(numloops, numtrans);
delave=zeros(numtrans,1);
deldev=zeros(numtrans,1);
rmsqdd=zeros(numtrans,1);
delsq=zeros(numloops, numtrans);
sqdev=zeros(numloops,numtrans);
for i=1:numtrans
    % for gamma value analysis
    gamave(i)=sum(gamstor(:,i,1))/numloops;
    gamdev(i)=sum(gamstor(:,i,2))/numloops;
    % for mean square devaition calculation
    gamsq(:,i)=gamstor(:,i,1).^2;
    sqdev(:,i)=(gamsq(:,i)-gamave(i)^2);
    rmsqdg(i)=sqrt(sum(sqdev(:,i))/numloops);
    % for delta values analysis
    delave(i)=sum(delstor(:,i,1))/numloops;
    deldev(i)=sum(delstor(:,i,2))/numloops;
    % for mean square devaition calculation
    delsq(:,i)=delstor(:,i,1).^2;
    sqdev(:,i)=(delsq(:,i)-delave(i)^2);
    rmsqdd(i)=sqrt(sum(sqdev(:,i))/numloops);
end
%{
figure   % for averaged error estimates
    hold on
    plot(Jvalues,gammas,'k^-')
    errorbar(Jvalues, gamave, gamdev,'r*-')
    xlabel('R-branch J-value');
    ylabel(' \gamma cm^{-1}/atm');
    legend('Initial \gamma','Ave Fit Voigt \gamma')
    title('Average Fitted Voigt \gamma with S_v^N fixed') 
%}
    % Mean square deviations
figure
    hold on
    plot(Jvalues,gammas,'k^-')
    errorbar(Jvalues, gamave, rmsqdg,'r*-')
    xlabel('R-branch J-value');
    ylabel(' \gamma / (cm^{-1}/atm)');
    legend('Initial \gamma','Ave Fit Voigt \gamma')
    title('Average Voigt \gamma (Absorbance), Hitran S_v^N') 
    % Now the deltas 
figure
    hold on
    plot(Jvalues,deltas, 'k-^')
    axis([7 14 -0.02 0.01])
    err=rmsqdd;         % RMS average errors
   % err=deldev;           % average errors returned from fits
    errorbar(Jvalues, fitdels(:,1),err,'r*-')
    xlabel('R-branch J-value');
    ylabel(' \delta / (cm^{-1}/atm)');
    legend('Initial \delta','Fitted Voigt \delta')
    title('Fitted Voigt \delta , Hitran S_v^N') 
%
%{
% Older code
gamstorsiz=size(gamstor);
numtrans=gamstorsiz(2);
gamave=zeros(numtrans,1);
gamdev=zeros(numtrans,1);
rmsqd=zeros(numtrans,1);
gamsq=zeros(numloops, numtrans);
for i=1:numtrans
    gamave(i)=sum(gamstor(:,i,1))/numloops;
    gamdev(i)=sum(gamstor(:,i,2))/numloops;
    % for mean square devaition calculation
    gamsq(:,i)=gamstor(:,i,1).^2;
    sqdev(:,i)=(gamsq(:,i)-gamave(i)^2);
    rmsqd(i)=sqrt(sum(sqdev(:,i))/numloops);
end
% Root Mean square deviations
figure
    hold on
    plot(Jvalues,gammas,'k^-')
    errorbar(Jvalues, gamave, gamdev,'r*-')
    xlabel('R-branch J-value');
    ylabel(' \gamma / (cm^{-1}/atm)');
    legend('Initial \gamma','Ave Fit Voigt \gamma')
    title('Average Voigt \gamma (Absorbance),  S_v^N fixed') 
% Mean square deviations
figure
    hold on
    plot(Jvalues,gammas,'k^-')
    errorbar(Jvalues, gamave, rmsqd,'r*-')
    xlabel('R-branch J-value');
    ylabel(' \gamma cm^{-1}/atm');
    legend('Initial \gamma','Ave Fit Voigt \gamma')
    title('Average Voigt \gamma (Absorbance),  RMS dev error') 
   
for i=1:numloops
    fitgams(:,:)=gamstor(i,:,:);
    firdels(:,:)=delstor(i,:,:);
    figure
    hold on
    plot(Jvalues,gammas,'k^-')
    err=fitgams(:,2);
    errorbar(Jvalues, fitgams(:,1), err,'r*-')
    xlabel('R-branch J-value');
    ylabel(' \gamma cm^{-1}/atm');
    legend('Initial \gamma','Fitted Voigt \gamma')
    title('Fitted Voigt \gamma with S_v^N fixed')   % Change as you like...
% Now the deltas
  
    figure
    hold on
    plot(Jvalues,deltas, 'k-^')
    axis([7 14 -0.02 0.0])
    err=fitdels(:,2);
    errorbar(Jvalues, fitdels(:,1),err,'r*-')
    xlabel('R-branch J-value');
    ylabel(' \delta cm^{-1}/atm');
    legend('Initial \delta','Fitted Voigt \delta')
    title('Fitted Voigt \delta with S_v^N fixed') 
%} 
% end
