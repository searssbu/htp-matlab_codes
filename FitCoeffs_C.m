function [fitV, fitgams, offsets, fitU, fitdels, offsetsdel,fitW,fitg2s,offsetsg2] = ...
    FitCoeffs_C(numtrans, pressvals, finalparm)
%
% Take data from the Voigt fits to the synthetic QSDV and look for
% linearity to extract the Voigt broadening coeffcients.  This version will
% take the arrays of data for different transitions created by the
% HTP_FittingCode_suppl_C script.  These contain the 3-dimensional arrays
% of data for (parameter, pressure, transition)
% finalparm contains the Voigt fit data for pressures specified in the
% array pressvals, typically this has 5 numbers 10, 20, 40, 70, 100 torr.
% But can be anything, of course.
%!synclient HorizTwoFingerScroll=0
%%
%numpress = numel(pressvals);
fitgams = zeros(numtrans,2);
offsets = zeros(numtrans,2);
atm = pressvals/760;
atm=atm';
%
%% Gamma Analysis
for itran=1:numtrans
                                    % itran = 1;
   Gamma = finalparm(1,:,itran)'; 
   testG=Gamma./atm;
% Now plot the Voigt Gamma versus Pressure in atmospheresnumpress,1);
   fitV = fit(atm, Gamma,'poly1');
   P=coeffvalues(fitV);
   SDErrors=confint(fitV);
   SDs=P-SDErrors(1,:);
   model=P(1)*atm+P(2);
   Deviation=Gamma-model;
   %{
   figure;
   plot(atm, Gamma,'r+', atm,10*Deviation,'k-');
   hold on;
   legend('data','10*deviation','location', 'northwest');
   plot(fitV,'b-');
   xlabel('Pressure/atm');
   ylabel('Fitted Voigt \Gamma');
   %sprintf('Parameter P(1)=%8.5e +/- %5.1e\n',P(1),SDs(1))
   %sprintf('Parameter P(2)=%8.5e +/- %5.1e\n',P(2),SDs(2))
   %}
   fitgams(itran,1) = P(1);
   fitgams(itran,2) = SDs(1);
   offsets(itran,1) = P(2);
   offsets(itran,2) = SDs(2);
%
end                                           % of loop over transtions
%
%% delta (shift) analysis
fitdels = zeros(numtrans,2);
offsetsdel = zeros(numtrans,2);
for itran=1:numtrans
                                    % itran = 1;
   Delta = finalparm(2,:,itran)'; 
% Now plot the Voigt Shift versus Pressure in atmospheresnumpress,1);
   fitU = fit(atm, Delta,'poly1');
   P=coeffvalues(fitU);
   SDErrors=confint(fitU);
   SDs=P-SDErrors(1,:);
   model=P(1)*atm+P(2);
   Deviation=Delta-model;
   %{
   figure;
   plot(atm, Delta,'r+', atm,10*Deviation,'k-');
   hold on;
   legend('data','10*deviation','location', 'northwest');
   plot(fitV,'b-');
   xlabel('Pressure/atm');
   ylabel('Fitted Voigt \Delta');
   %sprintf('Parameter P(1)=%8.5e +/- %5.1e\n',P(1),SDs(1))
   %sprintf('Parameter P(2)=%8.5e +/- %5.1e\n',P(2),SDs(2))
   %}
   fitdels(itran,1) = P(1);
   fitdels(itran,2) = SDs(1);
   offsetsdel(itran,1) = P(2);
   offsetsdel(itran,2) = SDs(2);
%
end                                           % of loop over transtions
%% gamma_2 (SD brooadening) analysis
fitg2s = zeros(numtrans,2);
offsetsg2 = zeros(numtrans,2);
for itran=1:numtrans
                                    % itran = 1;
   Gam2 = finalparm(3,:,itran)'; 
   test=Gam2./atm;
% Now plot the g_2's versus Pressure in atmospheresnumpress,1);
   fitW = fit(atm, Gam2,'poly1');
   P=coeffvalues(fitW);
   SDErrors=confint(fitW);
   SDs=P-SDErrors(1,:);
   model=P(1)*atm+P(2);
   Deviation=Gam2-model;
   %{
   figure;
   plot(atm, Gam2,'r+', atm,10*Deviation,'k-');
   hold on;
   legend('data','10*deviation','location', 'northwest');
   plot(fitV,'b-');
   xlabel('Pressure/atm');
   ylabel('Fitted Voigt \Delta');
   %sprintf('Parameter P(1)=%8.5e +/- %5.1e\n',P(1),SDs(1))
   %sprintf('Parameter P(2)=%8.5e +/- %5.1e\n',P(2),SDs(2))
   %}
   fitg2s(itran,1) = P(1);
   fitg2s(itran,2) = SDs(1);
   offsetsg2(itran,1) = P(2);
   offsetsg2l(itran,2) = SDs(2);
%
end                                           % of loop over transtions

end
